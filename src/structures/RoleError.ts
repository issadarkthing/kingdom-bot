import { CommandError } from "@jiman24/commandment";
import { User } from "discord.js";
import { Role } from "./Player";


export class RoleError extends CommandError {
  user: User;
  role: Role;

  constructor(user: User, role: Role) {
    super();

    this.user = user;
    this.role = role;
    this.message = `${user} Only ${this.role} can use this command`;
  }
}

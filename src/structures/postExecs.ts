import { Message } from "discord.js";
import { GuildMember } from "discord.js";
import { LORD_ROLE_ID, TEST_LORD_ROLE_ID } from "./constants";
import { RoleError } from "./RoleError";

export function isMemberLord(member: GuildMember) {
  const lordRoles = [LORD_ROLE_ID, TEST_LORD_ROLE_ID];
  return member.roles.cache.some(x => lordRoles.includes(x.id));
}

export function isFarmer(msg: Message) {
  if (isMemberLord(msg.member!)) {
    throw new RoleError(msg.author, "Farmer");
  }
}

export function isLord(msg: Message) {
  if (!isMemberLord(msg.member!)) {
    throw new RoleError(msg.author, "Lord");
  }
}

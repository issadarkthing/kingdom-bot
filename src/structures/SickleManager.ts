import Josh from "@joshdb/core";
//@ts-ignore
import provider from "@joshdb/sqlite";

interface Data {
  from: string;
  to: string;
  date: Date;
}

export class SickleManager {
  private readonly id = "main";
  private db = new Josh<Data[]>({
    name: "sickle",
    provider,
  });

  async giveSickle(from: string, to: string) {

    const data = { from, to, date: new Date() };
    const main = await this.db.ensure(this.id, []);

    main.push(data);
    await this.db.set(this.id, main);
  }

  // to check the amount of sickles given
  async sicklesGiven(from: string) {
    const sickles = await this.db.get(this.id) || [];
    return sickles.filter(x => x.from === from).length;
  }

  async getSickle(to: string) {
    const sickles = await this.db.get(this.id) || [];
    return sickles.find(x => x.to === to);
  }

  /** Validate if the lord gave the sickle to the farmer */
  async validateSickleGive(from: string, to: string) {
    const data = await this.db.get(this.id) || [];
    return data.some(x => x.from === from && x.to === to);
  } 

  async takeSickle(from: string, to: string) {
    await this.db.remove(this.id, (val: Data) => val.from === from && val.to === to);
  }

  /** Returns sickles given to farmers */
  async getSickles(from: string) {
    const sickles = await this.db.get(this.id) || [];
    return sickles.filter(x => x.from === from);
  }
}

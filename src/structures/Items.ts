

export interface Item {
  id: string;
  name: string;
}

export const items: Item[] = [
  { 
    id: "cow_shaped_broken_pottery",
    name: "A piece of broken pottery shaped like a cow",
  },
  {
    id: "lump_of_soil",
    name: "A lump of soil",
  },
  {
    id: "old_longsword",
    name: "The weathered hilt of an old longsword",
  },
  {
    id: "apple_core",
    name: "An apple core",
  },
  {
    id: "nothing",
    name: "nothing, you were imagining things",
  }
];


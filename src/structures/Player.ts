import { User } from "discord.js";
import { player as playerDB } from "../bot";
import { DateTime } from "luxon";

interface CommandData {
  count: number;
  lastUsed: Date;
}

interface Penalty {
  expiry: Date;
  isOnPenalty: boolean;
}

export const MAX_SICKLE = 5;
export const MAX_COMMAND_COUNT = 1;
export const COOLDOWN = 5; // hours
export const XP = 10;

export function isPassed(dt: DateTime | Date) {

  if (dt instanceof Date) {
    dt = DateTime.fromJSDate(dt);
  }

  return dt.diffNow(["seconds"]).seconds < 0;
}

export function timeLeft(dt: DateTime | Date) {

  if (dt instanceof Date) {
    dt = DateTime.fromJSDate(dt);
  }

  const { 
    hours, 
    minutes, 
  } = dt.diffNow(["hours", "minutes", "seconds"]);

  return `${hours}h ${minutes}m`
}

export type Role = "Farmer" | "Lord";

export class Player {
  id: string;
  name: string;
  xp: number = 0;
  wheat: number = 0;
  gold: number = 0;
  ownSickle: boolean = false;
  items: string[] = [];

  harvest: CommandData = {
    count: 0,
    lastUsed: new Date(2000),
  };

  beg: CommandData & { penalty: Penalty } = {
    count: 0,
    lastUsed: new Date(2000),
    penalty: {
      isOnPenalty: false,
      expiry: new Date(2000),
    }
  }

  constructor(id: string, name: string) {
    this.id = id;
    this.name = name;
  }

  static async fromUser(user: User) {
    const player = new Player(user.id, user.username);
    const data = await playerDB.get(player.id);

    Object.assign(player, data);

    return player;
  }

  // should not be used when there is possiblity user does not exists
  static async fromID(id: string) {
    const player = new Player(id, "");
    const data = await playerDB.get(id);

    Object.assign(player, data);

    return player;
  }

  isOnCooldown(commandData: CommandData) {
    return commandData.count >= MAX_COMMAND_COUNT;
  }

  isCooldownFinished(commandData: CommandData) {
    const dt = DateTime.fromJSDate(commandData.lastUsed);
    const expiryTime = dt.plus({ hours: COOLDOWN });
    return isPassed(expiryTime);
  }

  cooldownTimeLeft(commandData: CommandData) {
    const expiry = DateTime.fromJSDate(commandData.lastUsed).plus({ hours: COOLDOWN });
    return timeLeft(expiry);
  }

  async save() {
    await playerDB.set(this.id, { ...this });
  }
}

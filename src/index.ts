import { client } from "./bot";
import { config } from "dotenv";

config();

client.login(process.env.BOT_TOKEN);

import { Message } from "discord.js";
import { Command } from "@jiman24/commandment";
import { Player } from "../structures/Player";

export default class extends Command {
  name = "xp";
  disable = true;

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);
    const { xp } = player;
    
    msg.channel.send(`${msg.author} You current xp is **${xp}**`);

  }
}

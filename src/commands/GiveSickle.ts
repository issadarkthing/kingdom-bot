import { Message, User } from "discord.js";
import { Command, CommandError, Exec } from "@jiman24/commandment";
import { DurationLikeObject } from "luxon";
import { MAX_SICKLE, Player } from "../structures/Player";
import { sickleManager } from "../bot";
import { random } from "@jiman24/discordjs-utils";
import { isMemberLord } from "../structures/postExecs";

export default class extends Command {
  name = "givesickle";
  cooldown?: DurationLikeObject | undefined = { hours: 5 };

  response(from: User, to: User) {
    return random.pick([
      `You think back on your days as a farmer. They seem so long ago. Though you love the life of a lord, you think back to the memories you had on the farm with ${to} and invite them to join your noble house. You give them a personally engraved sickle. On the side it reads: "I always joked about being your boss." ${to} receives 1 Sickle from ${from}.`,
      `During your ride past the fields, you notice a struggling farmer. You ask their name. "${to}," they mumble. You reach into your saddlebag and gift them a new sickle. "Use it well," you reply. "I only ask you return the favor at the market". ${to} receives 1 Sickle from ${from}.`,
      `You receive word local workers are being mistreated by a rival lord. Their leader, ${to} arrives and demands better working conditions. "I like your spirit," you reply. "Come work for me instead." ${to} receives 1 Sickle from ${from}.`,
      `On your tour through the nearby fields, you spot ${to} tending to the crops. Their tools are dull and their work is slow. "Here," you reply. "This will work much better." ${to} receives 1 Sickle from ${from}.`,
      `You made a bet with your childhood rival ${to} one day you'd become a lord. Now they curse your name, but you decide to use their anger to your benefit. "Take this and think of me each time you swing it". You smile at your cleverness as their harvesting pace increases. ${to} receives 1 Sickle from ${from}.`,
      `The farmers whisper rumors of wandering beasts which plague the fields. One particularly cowardly farmer, ${to}, has not slept in five days. You pity their plight and decide to ease their fears with a gift. "It's dangerous to go alone. Take this." ${to} receives 1 Sickle from ${from}.`,
      `You are a lord, but you are not without humor. You don a black cloak and hide in the barn, waiting for someone to arrive. ${to} opens the door. You leap out and proclaim "I am Death! I have come for your soul!" They faint on the spot. You leave your weapon in their hand and depart with a laugh. ${to} receives 1 Sickle from ${from}.`,
      `The crop is in full season now. Happy workers make productive workers. You point at the nearest farmer, ${to}, and bestow a fine tool to help them on their way. They thank you profusely and swear to return the favor. ${to} receives 1 Sickle from ${from}.`,
      `You decide perhaps a generous gift will repay your coffers tenfold. You hail ${to} from your carriage. Their eyes shine brightly as you offer them a gift. "I won't forget this!" they cry happily. ${to} receives 1 Sickle from ${from}.`,
      `You remember your days in the fields working beside ${to} and wonder how they are. You visit them, only to discover their tools are worn and weathered. "You deserve better," you confess, and hand them a finely crafted sickle. "I only ask a small portion in return." ${to} receives 1 Sickle from ${from}.`,
    ]);
  }

  async exec(msg: Message) {

    if (!isMemberLord(msg.member!)) {
      throw new CommandError(`${msg.author} You are a farmer - you don't have any sickles to give away!`);
    }

    const player = await Player.fromUser(msg.author);
    const member = msg.mentions.members?.first();

    if (!member) {
      throw new CommandError(`${msg.author} You need to mention a user`);
    }

    const sicklesGiven = await sickleManager.sicklesGiven(player.id);

    if (sicklesGiven >= MAX_SICKLE) {
      throw new CommandError(`${msg.author} You can recruit up to ${MAX_SICKLE} farmers only`);
    }

    const farmer = await Player.fromUser(member.user);

    if (isMemberLord(member)) {
      throw new CommandError(`${msg.author} Giving a sickle to another Lord would be an insult!`);
    } else if (farmer.ownSickle) {
      throw new CommandError(`${msg.author} This farmer has already own a sickle`);
    }

    sickleManager.giveSickle(player.id, member.id);

    farmer.ownSickle = true;
    farmer.save();

    const message = this.response(msg.author, member.user);

    msg.channel.send(`${msg.author} ${message}`);
  }
}

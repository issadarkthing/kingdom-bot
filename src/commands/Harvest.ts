import { Message } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { random } from "@jiman24/discordjs-utils";
import { Player } from "../structures/Player";
import { DurationLikeObject } from "luxon";
import { isMemberLord } from "../structures/postExecs";

export default class extends Command {
  name = "harvest";
  cooldown?: DurationLikeObject = { hours: 5 };

  harvestItemResponse(itemName: string) {
    const text = random.pick([
      `You look at your field and think "What is this doing here...?"`,
      `Your sickle suddenly catches on something. Startled, you look closer and hope you haven't harmed or damaged it.`,
      `There is a commotion of birds in the field. You run to the area, only to find something lying on the ground`,
      `Something catches your eye just beyond the gate. A lost item, perhaps? You walk over and pick it up.`,
      `A local dog runs up to you suddenly, tail wagging with an object in its mouth. You kneel down to take a closer look.`,
    ]);

    return `${text} You receive **${itemName}**`;
  }

  harvestWheatResponse(amount: number) {
    const text = random.pick([
      `You look at your yield and wonder if it will ever be worth something...`,
      `The heat of the sun is punishing as you take to the fields.`,
      `You hear the sound of a distant storm rolling in. You gather the remainder of your crop and head home.`,
      `You're not the only one harvesting the fields. Others have come and gone, some making notable profits. You hope luck will smile upon you.`,
      `You hear the sound of chickens mingling beneath a nearby windmill as you carry your harvested bundle.`,
      `You notice a sack left by the barn. "What is this doing here...?"`,
      `Your sickle shines in the sunlight as it slices through the stalks.`,
      `There is a commotion of birds in the field. You run to the area, only to find a bundle lying on the ground.`,
      `Something catches your eye just beyond the gate. A lost bag, perhaps? You walk over and pick it up.`,
      `A local dog runs up to you suddenly, tail wagging with a pouch in its mouth. You kneel down to take a closer look.`,
    ]);

    return `${text} You receive **${amount}** Wheat`;
  }


  async exec(msg: Message) {

    if (isMemberLord(msg.member!)) {
      throw new CommandError(`${msg.author} Harvesting is not a job fit for a LORD!`);
    }

    const player = await Player.fromUser(msg.author);
    const wheatAmount = player.ownSickle ? random.integer(20, 40) : random.integer(0, 20);
    const message = this.harvestWheatResponse(wheatAmount);

    player.wheat += wheatAmount;

    await player.save();

    msg.channel.send(`${msg.author} ${message}`);
  }
}

import { Message } from "discord.js";
import { Command } from "@jiman24/commandment";
import { Player } from "../structures/Player";
import { items } from "../structures/Items";
import { toNList } from "@jiman24/discordjs-utils";

export default class extends Command {
  name = "items";
  disable: boolean = true;

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);
    const itemNames = player.items.map(id => items.find(x => x.id === id)!);

    const message = `Items:\n${toNList(itemNames.map(x => x.name))}`;

    msg.channel.send(`${msg.author} ${message}`);
  }
}

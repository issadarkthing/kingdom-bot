import { Message } from "discord.js";
import { Command, CommandError, Exec } from "@jiman24/commandment";
import { random } from "@jiman24/discordjs-utils";
import { isPassed, Player, timeLeft } from "../structures/Player";
import { DateTime, DurationLikeObject } from "luxon";
import { isFarmer } from "../structures/postExecs";

type BegOutput = "wheat" | "item" | "penalty";

export default class extends Command {
  name = "beg";
  cooldown?: DurationLikeObject = { hours: 5 };
  penaltyTime = 20; // hours
  description = "To do something";
  preExec: Exec[] = [isFarmer];

  begWheatResponse(amount: number) {
    const text = random.pick([
      `You beg to the local Lord that he might let your quotas slide this summer. He pities you.`,
      `You are weak from hunger and can't remember when your last meal was. You beg the other Farmers to help you.`,
      `You spot the local Lord's horseman nearby and honor their presence with a song. This pleases them, and they offer a reward from their stores.`,
      `The day is too beautiful to ignore. You beg your fellow Farmers to help you out so you may take a break and enjoy it.`,
      `Work in the fields has left you exhausted. You desperately appeal to the locals to assist your efforts`,
      `The local Lord is generous. You are fortunate to receive support.`,
      `You hear rumors something was found hidden within the barn. You investigate and find a forgotten sack behind the hay bales.`   ,
      `You see a stranger leave a generous donation for a nearby beggar. You decide to appeal to the stranger's kindness, too.`   ,
      `You are tired of Wheat, and beg the Head Farmer for a different form of payment. He laughs.`   ,
      `You are hungry, and spot a group of travelers on the road beside the field. You rush toward them and beg for aid, hoping fortune is with you.`,
    ]);

    return `${text} You receive **${amount}** Wheat`
  }

  begItemResponse(itemName: string) {
    const text = random.pick([
      `You beg to the local Lord that he might let your taxes slide this summer. He is firm, yet strangely generous.`,
      `You hear rumors something was found hidden within the barn. You beg the Head farmer to let you have it.`,
      `You see a stranger leave a generous donation for a nearby beggar. You decide to appeal to the stranger's kindness, too.`,
      `You are tired of Wheat, and beg the Head farmer for a different form of payment.`,
      `You spot a group of travelers on the road beside the field. You rush toward them and beg for aid, hoping fortune is with you.`
    ]);

    return `${text} You receive **${itemName}**`;
  }

  penaltyResponse(amount: number) {
    const text = random.pick([
      `You beg the local Lord that he might let your taxes slide this summer. His face turns red with rage and the guards chase you out of town.`,
      `The other Farmers have grown tired of your begging and report to local Lord's guards.`,
      `Your whistling in the fields disturbs a passing guard's horse. The guard is thrown from the steed, and in turn you are thrown in the stocks.`,
      `Your incessant begging has upset the locals. They lock you in the barn as you gather your tools, trapping you inside.`,
      `Your pleas for help have fallen on deaf ears. You sulk beside the lake just out of town. The fish care nothing for your woes.`,
      `Ask and ye shall receive. Ask too much and ye shall receive punishment.`,
      `Someone in town has reported you for impersonating the local Lord. You vanish into the shadows in hopes the guards will not find you.`,
      `The act of begging has dried your throat. Your voice quiets as you search for water and attempt to recover.`,
      `The local Lord orders you for gagged for begging and suggests you try working next time.`,
      `It seems fortune favors someone other than you.`,
    ]);

    return `${text} You may not beg for **${amount}** Hours`;
  }

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    if (
      player.beg.penalty.isOnPenalty &&
      !isPassed(player.beg.penalty.expiry)
    ) {

      const { expiry } = player.beg.penalty;
      throw new CommandError(`${msg.author} You are on penalty. Please wait for **${timeLeft(expiry)}**`);

    } else if (
      player.beg.penalty.isOnPenalty &&
      isPassed(player.beg.penalty.expiry)
    ) {
      player.beg.penalty.isOnPenalty = false;

    }


    const wheatAmount = random.integer(0, 20);
    const begOutput = random.pick(["wheat", "penalty"] as BegOutput[]);

    const message = 
      begOutput === "wheat" ? this.begWheatResponse(wheatAmount) :
      begOutput === "penalty" ? this.penaltyResponse(this.penaltyTime) : "";


    if (begOutput === "wheat") {
      player.wheat += wheatAmount;

    } else if (begOutput === "penalty") {

      const penaltyTime = DateTime.now().plus({ hours: this.penaltyTime });

      player.beg.penalty.isOnPenalty = true;
      player.beg.penalty.expiry = penaltyTime.toJSDate();
    }

    await player.save();

    msg.channel.send(`${msg.author} ${message}`);
  }
}

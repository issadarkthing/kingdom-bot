import { Message } from "discord.js";
import { Command, CommandError } from "@jiman24/commandment";
import { Player } from "../structures/Player";
import { random } from "@jiman24/discordjs-utils";
import { isMemberLord } from "../structures/postExecs";
import { LORD_ROLE_ID, TEST_LORD_ROLE_ID } from "../structures/constants";
import { sickleManager } from "../bot";

export default class extends Command {
  name = "buyland";
  landCost = 30;

  response() {
    const text = random.pick([
      "The farm has been good to you. Your family is fed and happy. As winter draws near, you count your gold - the yields of your labor... You heard there was land for sale on the other side of the valley. You purchase the land as your own. You are now a [LORD].",
      "You have scrimped and saved your gold with care. Now, it's time to claim your fortune. You take your savings and purchase a fruitful piece of land on the other side of the valley. You are now a [LORD].",
      "The day you've been waiting for has arrived. With a skip in your step, you gather your gold and make haste to purchase your own parcel of land. You are now a [LORD].",
      "Your days of hard work and starvation are over. You take your savings and use them to purchase the deed to your own section of land. Will you be kind or cruel to the farmers who tend it? You are now a [LORD].",
      "You shake the dust from the brim of your hat. Soon the rough straw between your fingers will be swapped for silk and luxury. You smile, content you have achieved your dreams. You are now a [LORD].",
      "They told you it was impossible. You did not believe them. Through hard work and perseverance, you triumph over all the odds. The crown upon your head suits you. You are now a [LORD].",
      "The other farmers wave as you leave the familiar fields behind. Though the workload was great, the reward is greater still. You hope they will smile when next they see you again. You are now a [LORD].",
      "You once had a dream you owned your own land. As you exchange your gold for a deed, so too do you exchange your cloak for rich robes and bigger dreams still. You are now a [LORD].",
      "Your pulse quickens as you place your gold on the counter. You watch as the official documents are stamped and a deed is handed to you. You never thought this day would come. You are now a [LORD].",
      "A wise traveler once told you success was earned, not taken. You breathe in deeply as you step onto the land you have purchased. The air is sweet and full of promise. You are now a [LORD].",
    ]);

    return text;
  }

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    if (isMemberLord(msg.member!)) {
      throw new CommandError(`${msg.author} You already a lord!`);
    } else if (player.gold < this.landCost) {
      throw new CommandError(`${msg.author} Insufficient balance, you need ${this.landCost} Gold`);
    }

    player.gold -= this.landCost;

    player.save();

    const roles = await msg.guild?.roles.fetch();
    const lordRole = roles?.get(LORD_ROLE_ID) || roles?.get(TEST_LORD_ROLE_ID);

    if (!lordRole) {
      throw new CommandError(`Lord role not found`);
    }

    msg.member?.roles.add(lordRole);

    const sickle = await sickleManager.getSickle(player.id);

    // remove sickle if the farmer owns one
    if (sickle) {
      await sickleManager.takeSickle(sickle.from, player.id);
    }

    const message = this.response();

    msg.channel.send(`${msg.author} ${message}`);

  }
}

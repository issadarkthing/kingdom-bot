import { Message, User } from "discord.js";
import { Command, CommandError, Exec } from "@jiman24/commandment";
import { Player } from "../structures/Player";
import { sickleManager } from "../bot";
import { random } from "@jiman24/discordjs-utils";
import { isLord, isMemberLord } from "../structures/postExecs";

export default class extends Command {
  name = "takesickle";
  preExec: Exec[] = [isLord];

  response(to: User) {
    return random.pick([
      `Who on earth gave ${to} a recommendation to work on your land? You caught them drinking too much mead, singing songs of faraway lands to the pigs... at 4am... You take their sickle, tell them to to sleep it off, and send them on their way once they've recovered. ${to} no longer works for you.`,
      `You've received multiple reports of ${to} impersonating your mother. "My mother worked the fields with her bare hands. So too, shall you." You snatch the sickle you so generously gifted and order them taken away. ${to} no longer works for you.`,
      `Alas, not ${to} again! They stand beside a fallen tree, walking in circles, sitting and standing over and over. "Your path is broken," you proclaim, and sigh with remorse. ${to} no longer works for you.`,
      `A string of thefts has plagued the local market. The description of the thief bears a striking resemblance to ${to}. You decide it best to avoid a scandal. That afternoon, you take their sickle and order them banished from your farm. ${to} no longer works for you.`,
      `It seems anyone can pass for a farmer these days. Even a beggar like ${to}. "If you spent half as much time working as you do begging, it would not have come to this," you say as you take their sickle. ${to} no longer works for you.`,
      `There's something about ${to}. Their mere presence disturbs you in some unknown way. Perhaps it's their missing tooth. You simply cannot take looking at them another moment. You order them banished and their sickle reclaimed. ${to} no longer works for you.`,
      `You pride yourself on hiring only the finest farmers. ${to} is not one of them, it seems. You decide your reputation is far more important than their livelihood. Their sickle is taken and you demand they depart. ${to} no longer works for you.`,
      `There have been reports of a strange creature howling at the moon in your fields at night. You order your men to investigate. The next morning they return with ${to} in tow. The measly farmer reeks of ale and fouler liquids still. You order their sickle taken and them banished from your land. ${to} no longer works for you.`,
      `This is the last straw. ${to} has been bragging to the other farmers you let their taxes slide. Now all your workers want a break. It simply cannot stand. You remove their sickle and order them put in the stocks. ${to} no longer works for you.`,
      `You've heard rumors ${to} double-dips their bread at the tavern. "Outrageous!" you shout. "Does this look like the dark ages to you?" You order their sickle taken and have them banished for life. ${to} no longer works for you.`,
    ]);
  }

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);
    const member = msg.mentions.members?.first();

    if (!member) {
      throw new CommandError(`${msg.author} You need to mention a user`);
    }

    const farmer = await Player.fromUser(member.user);

    if (isMemberLord(member)) {
      throw new CommandError(`${msg.author} User you mentioned is not a farmer`);
    } else if (!farmer.ownSickle) {
      throw new CommandError(`${msg.author} The farmer does not own a sickle`);
    }

    const gaveSickle = await sickleManager.validateSickleGive(player.id, farmer.id);

    if (!gaveSickle) {
      throw new CommandError(`${msg.author} You didn't give the farmer a sickle`);
    }

    sickleManager.takeSickle(player.id, farmer.id);

    farmer.ownSickle = false;
    farmer.save();

    const message = this.response(member.user);

    msg.channel.send(`${msg.author} ${message}`);
  }
}

import { Message } from "discord.js";
import { Command, CommandError, Exec } from "@jiman24/commandment";
import { bold, random } from "@jiman24/discordjs-utils";
import { Player } from "../structures/Player";
import { sickleManager } from "../bot";
import { isFarmer } from "../structures/postExecs";

export default class extends Command {
  name = "market";
  preExec: Exec[] = [isFarmer];
  tax = 0.2;

  withoutSickleResponse(amount: number) {
    const text = random.pick([
      `You approach Agnes' stall at the market and think fondly of all the time you spent with her in your youth. You show her your harvest. "I'll be out of gold at this rate if you keep it up!" she laughs.`,
      `The market bustles with life. Nearby, you see a man selling cabbages. You think to yourself wheat is a much less danger-prone crop, but you'd best sell it before something happens.`,
      `You make your way to the tavern beside the market square. The bartender smiles when you enter. "Your crop was quite popular in our last brew!" he proclaims.`,
      `You've grown tired of the color of wheat. Gold is a much better hue to line your pockets.`,
      `The smell of freshly-baked loaves fill the air. Perhaps the baker will make you a loaf if you sell them your wares.`,
      `The scent of the market's fresh crackling honey ham makes your mouth water. You are eager to sell your harvest so that you may purchase a portion.`,
      `The laughter of children weaves its way through the market crowd. Ah, to be young again—but to be paid is better still. You throw your bundle on the counter of the nearest shopkeeper.`,
      `The market is awash with vibrant colors you could only dream of back at the farm. "Gold is much better than wheat", you decide as you sell your wares.`,
      `An elderly man beckons to you from his stall.  "I was a farmer for years, and know the look of someone down on their luck," he says, and offers to buy your crops.`,
      `A giant man shouts at you from the baker's stand. He grumbles he is out of wheat and demands to buy your crop. You fear if you refuse, he may grind your bones into bread. In exchange for your bundle, he drops a coin pouch on the counter.`
    ]);

    return `${text} You receive **${amount}** Gold`;
  }

  withSickleResponse(receive: number, tax: number, lord: string) {
    const text = random.pick([
      `You trudge clumsily through town with your weekly yield in tow. The sun is setting and it's time to pay your taxes. The merchant laughs as you stumble toward his stall. "I thought I'd stay open five minutes longer, late again!" He exclaims.`,
      `The guards at the market approach you. They remind you were it not for the local Lord, you would not yield so handsome a crop. You nod quickly, scurrying off to sell your wares.`,
      `A bard serenades you as you enter the market, courtesy of the local Lord. Their tune rings pleasantly in your ears as you offer your wares to the baker. Entertainment this grand doesn't pay for itself, they remind you.`,
      `You are filled with joy to receive full pay for the crops you harvested from the fields. Yet the glint of the sickle on your belt catches your eye and reminds you of your obligations.`,
      `You spot a woman named Beatrice at the market stand. "Sold your soul for a sickle?" she chides. Your smile is bitter as you sell your wares.`,
      `The blacksmith's eyes ignite when he sees the sickle you carry. "Now that's a proper tool!" he exclaims. "The Lord themselves commissioned me for it. You'd best pay them for their generosity!" You smile coldly as your profits are diminished.`,
      `It has been a long day. Only exchanging your wheat for coin will lift your spirits. Yet the profits are lighter than you expected.`,
      `The only things certain in life are death and taxes. You are thankful the former has not yet knocked at your door, and it cushions the impact of the latter when you sell your wheat.`,
      `As you look around the market, you wonder how much gold must exchange hands here. You watch as some is deducted from your sale, yet the majority is still yours.`,
      `This is a very nice market, isn't it? And a very nice crop you've collected, hm? It would be a shame if something happened to it. You quickly put the thought out of your mind as you sell your wheat.`,
    ]);

    return `${text} You receive ${bold(receive)} Gold and pay the Lord ${lord} ${bold(tax)} Gold in taxes.`
  }

  async exec(msg: Message, args: string[]) {

    const conversionRate = 0.1; // 1 wheat -> 0.1 gold
    const amount = parseInt(args[0]);
    const player = await Player.fromUser(msg.author);

    if (!args[0]) {
      throw new CommandError(`${msg.author} How much wheat would you like to sell?`);
    } else if (Number.isNaN(amount)) {
      throw new CommandError(`${msg.author} You need to give a valid amount`);
    } else if (amount < 1) {
      throw new CommandError(`${msg.author} You need to give amount more than 0`);
    } else if (amount % 50 !== 0) {
      throw new CommandError(`${msg.author} You can only sell in the multiple of 50 wheats`);
    } else if (amount > player.wheat) {
      throw new CommandError(`${msg.author} Insufficient balance`);
    }

    const gold = conversionRate * amount;

    player.wheat -= amount;

    if (player.ownSickle) {

      const sickle = (await sickleManager.getSickle(player.id))!;
      const lord = await Player.fromID(sickle.from);
      const tax = Math.round(gold * this.tax);
      const netGold = gold - tax;

      player.gold += netGold;
      await player.save();

      lord.gold += tax;
      await lord.save();

      const lordUser = `<@${lord.id}>`;
      const message = this.withSickleResponse(netGold, tax, lordUser);

      msg.channel.send(`${msg.author} ${message}`);

      return;
    }

    const message = this.withoutSickleResponse(gold);

    player.gold += gold;

    await player.save();

    msg.channel.send(`${msg.author} ${message}`);
  }
}

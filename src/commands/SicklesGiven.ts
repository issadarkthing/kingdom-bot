import { Message } from "discord.js";
import { Command, Exec } from "@jiman24/commandment";
import { isLord } from "../structures/postExecs";
import { sickleManager } from "../bot";
import { toNList } from "@jiman24/discordjs-utils";

export default class extends Command {
  name = "sicklesgiven";
  preExec: Exec[] = [isLord];

  async exec(msg: Message) {

    const sickles = await sickleManager.getSickles(msg.author.id);
    const guild = msg.guild!;

    await guild.members.fetch();

    const farmers: string[] = [];

    for (const sickle of sickles) {
      
      const member = guild.members.cache.get(sickle.to);
      const name = member?.displayName || `user ${sickle.to} is no longer in the server`;
      farmers.push(name);

    }

    msg.channel.send(`${msg.author}\n${toNList(farmers)}` || `${msg.author} You have not given sickle to any farmers`);
  }
}

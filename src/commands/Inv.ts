import { Message } from "discord.js";
import { Command } from "@jiman24/commandment";
import { Player } from "../structures/Player";

export default class extends Command {
  name = "inv";

  async exec(msg: Message) {

    const player = await Player.fromUser(msg.author);

    msg.channel.send(`${msg.author} You have ${player.wheat} Wheat and ${player.gold} Gold!`);

  }
}

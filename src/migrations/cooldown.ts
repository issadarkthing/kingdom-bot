import type { Player } from "../structures/Player";
import Josh from "@joshdb/core";
//@ts-ignore
import provider from "@joshdb/sqlite";
import { DateTime } from "luxon";

const db = new Josh({
  name: "cooldowns",
  provider,
});

const player = new Josh({
  name: "players",
  provider,
});

async function main() {

  const players = await player.values as Player[];

  console.log("migrating cooldowns");

  const convertTime = (dt: Date) => {
    return DateTime
      .fromJSDate(dt)
      .plus({ hours: 5 })
      .toJSDate();
  }
  
  let i = 0;

  for (const player of players) {

    const begExpiry = convertTime(player.beg.lastUsed);
    const harvestExpiry = convertTime(player.harvest.lastUsed);
    
    if (!player.beg.penalty.isOnPenalty) {
      await db.set(`beg-${player.id}`, begExpiry);
    }

    await db.set(`harvest-${player.id}`, harvestExpiry);

    i++;
  }

  console.log(`migrated ${i} data`);
}

main();


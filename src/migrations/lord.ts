import { client } from "../bot";
import { Player } from "../structures/Player";
import { isMemberLord } from "../structures/postExecs";
import { config } from "dotenv";
import { SERVER_ID } from "../structures/constants";

config();


console.log("removing 30 gold from lords");


client.on("ready", async () => {

  console.log("fetching guilds");
  await client.guilds.fetch();
  const guild = client.guilds.cache.get(SERVER_ID);

  if (!guild) {
    throw new Error("guild not found");
  }

  console.log("fetching members");
  await guild.members.fetch();
  const lords = guild.members.cache.filter(x => isMemberLord(x)).values();

  for (const lord of lords) {
    
    const player = await Player.fromUser(lord.user);
    const cost = 30;

    if (player.gold > cost) {
      player.gold -= cost;
    } else {
      player.gold = 0;
    }

    await player.save();

    console.log(`Deducted 30 gold from ${player.name}`);
  }

  console.log("process completed");

  client.destroy();
})


client.login(process.env.BOT_TOKEN);

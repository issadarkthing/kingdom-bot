import { isMemberLord } from "../structures/postExecs";
import { SERVER_ID } from "../structures/constants";
import { config } from "dotenv";
import { client, sickleManager } from "../bot";
import { Player } from "../structures/Player";

config();

console.log("removing sickles...");


client.on("ready", async () => {

  console.log("fetching guilds");
  await client.guilds.fetch();
  const guild = client.guilds.cache.get(SERVER_ID);

  if (!guild) {
    throw new Error("guild not found");
  }

  console.log("fetching members");
  await guild.members.fetch();
  const lords = guild.members.cache.filter(x => isMemberLord(x)).values();

  for (const lord of lords) {
    
    const sickle = await sickleManager.getSickle(lord.id);

    if (!sickle) continue;

    await sickleManager.takeSickle(sickle.from, sickle.to);
    
    console.log(`Removed sickle from ${lord.id} given by ${sickle.from}`);
  }

  console.log("process completed");

  client.destroy();
})


client.login(process.env.BOT_TOKEN);

import { Client } from "discord.js";
import { CommandError, CommandManager } from "@jiman24/commandment";
import path from "path";
import Josh from "@joshdb/core";
//@ts-ignore
import provider from "@joshdb/sqlite";
import { SickleManager } from "./structures/SickleManager";

export const player = new Josh({
  name: "players",
  provider,
});
export const sickleManager = new SickleManager();
export const client = new Client({ intents: ["GUILDS", "GUILD_MESSAGES", "GUILD_MEMBERS"] });

const COMMAND_PREFIX = process.env.PREFIX || "!";
const commandManager = new CommandManager(COMMAND_PREFIX);

commandManager.registerCommands(path.resolve(__dirname, "./commands"));

commandManager.registerCommandErrorHandler((error, msg) => {

  if (error instanceof CommandError) {
    msg.channel.send(error.message);

  } else {
    console.error(error);
  }
})

commandManager.registerCommandNotFoundHandler((msg, cmdName) => {
  msg.channel.send(`Cannot find command "${cmdName}"`);
})

commandManager.registerCommandOnCooldownHandler((msg, cmd, timeLeft) => {
  const { hours, minutes } = timeLeft;
  const fmtTimeleft = `${hours}h ${minutes}m`;
  msg.channel.send(`${msg.author} You are on cooldown **${fmtTimeleft}**`);
})

client.on("ready", () => console.log(client.user?.username, "is ready!"))
client.on("messageCreate", msg => commandManager.handleMessage(msg));
